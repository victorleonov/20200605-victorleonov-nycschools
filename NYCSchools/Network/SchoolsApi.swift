//
//  SchoolsApi.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import Foundation

class SchoolsApi: Api {
    var httpMethod: HTTPMethod = .get
    var endpoint: String = EnvConfig.endpoint(for: EnvConfig.ConfigKey.schoolsEndpoint)
    var urlParams: [URLQueryItem]?
    
    var limit: Int?
    var offset: Int?
    
    init(limit: Int? = nil, offset: Int? = nil) {
        self.limit = limit
        self.offset = offset
        
        var urlParams = [URLQueryItem]()
        if let limit = limit {
            urlParams.append(URLQueryItem(name: EnvConfig.ConfigKey.limitURLParam.rawValue, value: "\(limit)"))
        }
        if let offset = offset {
            urlParams.append(URLQueryItem(name: EnvConfig.ConfigKey.offsetURLParam.rawValue, value: "\(offset)"))
        }
        if urlParams.count > 0 {
            self.urlParams = urlParams
        }
    }
}
