//
//  EnvConfig.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import Foundation

/**
 Networking layer environment config struct.
*/
struct EnvConfig {
    /**
     Keys enum to access Info.plist configurations and URL query params.
    */
    enum ConfigKey: String {
        case baseURL = "BaseUrl"
        case endpoints = "Endpoints"
        case schoolsEndpoint = "schools"
        case scoresEndpoint = "scores"
        case limitURLParam = "$limit"
        case offsetURLParam = "$offset"
        case whereURLParam = "$where"
        case schoolIdURLParam = "dbn"
    }
    
    /**
     Host's base URL, BASE_URL needs to be defined in application target,
     then declared in Info.plist.
     
     - returns: Base URL string if exists in Info.plist, otherwise returns empty string.
    */
    static var baseURL: String = {
        return configValue(String.self, for: .baseURL) ?? ""
    }()
    
    /**
     An API endoint lookup in Info.plist.
    
     - parameter key: Endppoint key.
     - returns: Info.plist endpoint string if exists, otherwise returns empty string.
    */
    static func endpoint(for key: ConfigKey) -> String {
        return configValue([String: String].self, for: ConfigKey.endpoints)?[key.rawValue] ?? ""
    }
    
    private static func configValue<T: Hashable>(_ type: T.Type, for key: ConfigKey) -> T? {
        return Bundle.main.infoDictionary?[key.rawValue] as? T
    }
}
