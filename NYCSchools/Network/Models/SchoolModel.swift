//
//  SchoolModel.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import Foundation

struct SchoolModel: Codable {
    let id: String?
    let name: String?
    let address: String?
    let city: String?
    let zip: String?
    let state: String?
    let latitude: String?
    let longitude: String?
    let email: String?
    let website: String?
    
    private enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case address = "primary_address_line_1"
        case city
        case zip
        case state = "state_code"
        case latitude
        case longitude
        case email = "school_email"
        case website
    }
}
