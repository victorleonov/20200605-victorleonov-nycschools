//
//  SchoolScoresModel.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import Foundation

struct SchoolScoresModel: Codable {
    let id: String?
    let writing: Int?
    let reading: Int?
    let math: Int?
    
    private enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case writing = "sat_writing_avg_score"
        case reading = "sat_critical_reading_avg_score"
        case math = "sat_math_avg_score"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        
        let writingString = try values.decode(String.self, forKey: .writing)
        writing = Int(writingString)
        
        let readingString = try values.decode(String.self, forKey: .reading)
        reading = Int(readingString)
        
        let mathString = try values.decode(String.self, forKey: .math)
        math = Int(mathString)
    }
}
