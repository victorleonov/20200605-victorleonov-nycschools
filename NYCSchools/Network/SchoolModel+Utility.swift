//
//  SchoolModel+Utility.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import Foundation

// Convenience methods for SchoolModel
extension SchoolModel {
    /**
     Returns full addres string "," separated.
    */
    var fullAddress: String? {
        var str = ""
        if let s = address {
            str += s
        }
        // city
        if let s = city, s.count > 0 {
            str += str.count > 0 ? ", " : ""
            str += s
        }
        // state
        if let s = state, s.count > 0 {
            str += str.count > 0 ? ", " : ""
            str += s
        }
        // zip code
        if let s = zip, s.count > 0 {
            str += str.count > 0 ? ", " : ""
            str += s
        }
        return str.count > 0 ? str : nil
    }
    
    /**
     Checks if school matches to text.
    
     - parameter searchText: String to match school to.
     - returns: True if lowercasd text contains in any of name, address, city, zip,
     otherwise returns False.
    */
    func matches(searchText: String) -> Bool {
        return name?.lowercased().contains(searchText) == true ||
            address?.lowercased().contains(searchText) == true ||
            city?.lowercased().contains(searchText) == true ||
            state?.lowercased().contains(searchText) == true ||
            zip?.lowercased().contains(searchText) == true
    }
}
