//
//  SchoolScoresApi.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import Foundation

class SchoolScoresApi: Api {
    var httpMethod: HTTPMethod = .get
    var endpoint: String = EnvConfig.endpoint(for: EnvConfig.ConfigKey.scoresEndpoint)
    var urlParams: [URLQueryItem]? = [URLQueryItem]()
    
    init(schoolId: String) {
        let value = EnvConfig.ConfigKey.schoolIdURLParam.rawValue + "=" + "\"\(schoolId)\""
        self.urlParams = [URLQueryItem(name: EnvConfig.ConfigKey.whereURLParam.rawValue, value: value)]
    }
}
