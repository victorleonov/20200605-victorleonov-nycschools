//
//  SchoolScoresModel+Utility.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import Foundation

// Convenience methods for SchoolScoresModel
extension SchoolScoresModel {
    /**
     Returns True if model has value for any of the scores.
    */
    var hasScores: Bool {
        return math != nil || reading != nil || writing != nil
    }
}
