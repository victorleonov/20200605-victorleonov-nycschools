//
//  Api.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import Foundation

/**
 HTTP methods enum.
*/
enum HTTPMethod: String {
    case get = "GET"
}

/**
 API protocol.
*/
protocol Api {
    var httpMethod: HTTPMethod { get }
    var endpoint: String { get }
    var urlParams: [URLQueryItem]? { get }
}


extension Api {
    /**
     Builds URL from application's Base URL, API's endpoint and URL params if any.
     Returns nil if URL is malformed.
    */
    var requestURL: URL? {
        var urlComps = URLComponents(string: EnvConfig.baseURL)
        urlComps?.path = endpoint
        if let urlParams = urlParams {
            urlComps?.queryItems = urlParams
        }
        return urlComps?.url
    }
}
