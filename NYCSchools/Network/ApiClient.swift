//
//  ApiClient.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import Foundation

enum ApiClientError: Error {
    case invalidRequest
    case requestFailed
    case invalidResponse
}

class ApiClient<T: Decodable> {
    /**
     Asynchronous function executes provided API. Network call is made using shared URLSession.
    
     - parameter api: API object, used to configure URLRequest.
     - parameter completion: Closure will be called after request execution completes.
     - parameter responseObject: Response object of type T
     - parameter error: Execution error
    */
    static func execute(api: Api, _ completion: @escaping (_ responseObject: T?, _ error: ApiClientError?) -> Void) {
        // check if API url is valid, otherwise call completion with error
        guard let url = api.requestURL else {
            DispatchQueue.global().async {
                completion(nil, ApiClientError.invalidRequest)
            }
            return
        }
        // create request for url
        var request = URLRequest(url: url)
        request.cachePolicy = .returnCacheDataElseLoad
        // set API's HTTPMethod
        request.httpMethod = api.httpMethod.rawValue
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            // execution error, initialized with ApiClient
            //specific error .requestFailed if data task came back with error
            var taskError: ApiClientError? = error != nil ? .requestFailed : nil
            // execution object
            var object: T?
            do {
                // check if no prior error and if response data valid
                if taskError == nil, let data = data {
                    // decode data into object of requested type
                    object = try JSONDecoder().decode(T.self, from: data)
                }
            } catch {
                // decoding failed
                taskError = .invalidResponse
            }
            // complete with requested object and execution error
            completion(object, taskError)
        }.resume()
    }
}
