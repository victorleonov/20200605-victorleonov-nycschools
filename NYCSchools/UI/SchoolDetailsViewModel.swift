//
//  SchoolDetailsViewModel.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import Foundation
import CoreLocation

class SchoolDetailsViewModel {
    private let school: SchoolModel
    private(set) var scores: SchoolScoresModel?
    private(set) var isLoading = false
    
    var onScoresLoadFinished: (() -> Void)?
    
    init(schoolModel: SchoolModel) {
        self.school = schoolModel
    }
    
    var schoolTitle: String? {
        return school.name
    }
    
    var schoolAddress: String? {
        return school.fullAddress
    }
    
    var schoolEmail: String? {
        return school.email
    }
    
    var schoolWebsite: String? {
        return school.website
    }
    
    func getSchoolLocation() -> CLLocationCoordinate2D? {
        guard let latS = school.latitude,
            let lat = Double(latS),
            let lonS = school.longitude,
            let lon = Double(lonS)
        else { return nil }
        return CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }
    
    func loadSchoolScores() {
        guard isLoading == false, let id = school.id
        else {
            DispatchQueue.main.async {
                self.onScoresLoadFinished?()
            }
            return
        }
        
        let api = SchoolScoresApi(schoolId: id)
        ApiClient<[SchoolScoresModel]>.execute(api: api) { (objects, error) in
            self.loadScoresCompleted(objects?.first, error)
        }
    }
    
    func loadScoresCompleted(_ model: SchoolScoresModel?, _ error: Error?) {
        DispatchQueue.main.async {
            self.isLoading = false
            self.scores = model
            self.onScoresLoadFinished?()
        }
    }
}
