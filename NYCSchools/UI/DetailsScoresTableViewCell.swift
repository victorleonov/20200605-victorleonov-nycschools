//
//  DetailsScoresTableViewCell.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import UIKit

class DetailsScoresTableViewCell: UITableViewCell {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var scoresStackView: UIStackView!
    @IBOutlet weak var scores1Label: UILabel!
    @IBOutlet weak var scores2Label: UILabel!
    @IBOutlet weak var scores3Label: UILabel!
    
    func update(forModel model: SchoolScoresModel?, isLoading: Bool) {
        if let model = model {
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
            noDataLabel.isHidden = true
            scoresStackView.isHidden = false
            updateScoreLabels(for: model)
        } else {
            if isLoading {
                activityIndicator.startAnimating()
            }
            activityIndicator.isHidden = !isLoading
            noDataLabel.isHidden = isLoading
            scoresStackView.isHidden = true
        }
    }
    
    func updateScoreLabels(for model: SchoolScoresModel?) {
        guard let model = model, model.hasScores else {
            noDataLabel.isHidden = false
            return
        }
        update(scores1Label, withValue: model.math, prefix: "Math")
        update(scores2Label, withValue: model.reading, prefix: "Reading")
        update(scores3Label, withValue: model.writing, prefix: "Writing")
    }
    
    func update(_ label: UILabel, withValue value: Int?, prefix: String) {
        if let value = value {
            label.text = "\(prefix): \(value)"
        } else {
            label.isHidden = true
        }
    }
}
