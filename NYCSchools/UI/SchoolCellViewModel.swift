//
//  SchoolCellViewModel.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import Foundation

class SchoolCellViewModel {
    private(set) var school: SchoolModel!
    
    init(with school: SchoolModel) {
        self.school = school
    }
}
