//
//  DetailsAddressTableViewCell.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import UIKit
import MapKit

let mapRegionDistance = 500

class DetailsAddressTableViewCell: UITableViewCell, MKMapViewDelegate {
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var lastRegion: MKCoordinateRegion?
    var location: CLLocationCoordinate2D? {
        didSet {
            clearMapView()
            placeMapPin()
        }
    }
    
    func clearMapView() {
        let annotationsToRemove = mapView.annotations.filter { $0 !== mapView.userLocation }
        mapView.removeAnnotations(annotationsToRemove)
    }
    
    func placeMapPin() {
        guard let location = location else { return }
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        mapView.addAnnotation(annotation)
        
        if lastRegion == nil, let distance = CLLocationDistance(exactly: mapRegionDistance) {
            let region = MKCoordinateRegion(center: location, latitudinalMeters: distance, longitudinalMeters: distance)
            mapView.setRegion(mapView.regionThatFits(region), animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        lastRegion = mapView.region
    }
}
