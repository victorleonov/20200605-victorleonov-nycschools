//
//  DetailsContactsTableViewCell.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import UIKit

protocol DetailsContactsCellDelegate: class {
    func emailButtonTouched(in cell: DetailsContactsTableViewCell)
    func websiteButtonTouched(in cell: DetailsContactsTableViewCell)
}

class DetailsContactsTableViewCell: UITableViewCell {
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var websiteButton: UIButton!
    @IBOutlet weak var websiteStackView: UIStackView!
    
    weak var delegate: DetailsContactsCellDelegate?
    
    func update(withEmail email: String?, website: String?) {
        emailButton.setTitle(email, for: .normal)
        websiteButton.setTitle(website, for: .normal)
        emailStackView.isHidden = email == nil || email?.count == 0
        websiteStackView.isHidden = website == nil || website?.count == 0
    }

    @IBAction func emailButtonTouched(_ sender: Any) {
        delegate?.emailButtonTouched(in: self)
    }
    
    @IBAction func websiteButtonTouched(_ sender: Any) {
        delegate?.websiteButtonTouched(in: self)
    }
}
