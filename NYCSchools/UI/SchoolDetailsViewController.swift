//
//  SchoolDetailsViewController.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import UIKit

private enum SchoolDetailsTableCell: String {
    case title = "schoolTitleTableCellId"
    case address = "schoolAddressTableCellId"
    case contacts = "schoolContactTableCellId"
    case scores = "schoolScoresTableCellId"
}

private struct SchoolDetailsTableSection {
    let cells: [SchoolDetailsTableCell]
    var title: String?
}

class SchoolDetailsViewController: UITableViewController {
    var viewModel: SchoolDetailsViewModel!
    
    private var tableSections = [SchoolDetailsTableSection]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableSections = buildTableSections()
        bindViewModel()
        
        viewModel.loadSchoolScores()
    }
    
    func bindViewModel() {
        viewModel.onScoresLoadFinished = { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    fileprivate func buildTableSections() -> [SchoolDetailsTableSection] {
        var sections = [SchoolDetailsTableSection]()
        sections.append(SchoolDetailsTableSection(cells: [SchoolDetailsTableCell.title]))
        sections.append(SchoolDetailsTableSection(cells: [SchoolDetailsTableCell.address], title: "Address"))
        sections.append(SchoolDetailsTableSection(cells: [SchoolDetailsTableCell.scores], title: "SAT Scores"))
        sections.append(SchoolDetailsTableSection(cells: [SchoolDetailsTableCell.contacts]))
        return sections
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tableSections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableSections[section].cells.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return tableSections[section].title
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = tableSections[indexPath.section].cells[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId.rawValue, for: indexPath)
        if let cell = cell as? DetailsTextTableViewCell {
            cell.titleLabel.text = viewModel.schoolTitle
        }
        if let cell = cell as? DetailsAddressTableViewCell {
            cell.addressLabel.text = viewModel.schoolAddress
            cell.location = viewModel.getSchoolLocation()
        }
        if let cell = cell as? DetailsContactsTableViewCell {
            cell.delegate = self
            cell.update(withEmail: viewModel.schoolEmail, website: viewModel.schoolWebsite)
        }
        if let cell = cell as? DetailsScoresTableViewCell {
            cell.update(forModel: viewModel.scores, isLoading: viewModel.isLoading)
        }
        return cell
    }
}

extension SchoolDetailsViewController: DetailsContactsCellDelegate {
    func emailButtonTouched(in cell: DetailsContactsTableViewCell) {
        guard let email = viewModel.schoolEmail, email.count > 0 else { return }
        if let url = URL(string: "mailto:\(email)") {
          UIApplication.shared.open(url)
        }
    }
    
    func websiteButtonTouched(in cell: DetailsContactsTableViewCell) {
        guard let website = viewModel.schoolWebsite, website.count > 0 else { return }
        if let url = URL(string: "http://\(website)") {
            UIApplication.shared.open(url)
        }
    }
}
