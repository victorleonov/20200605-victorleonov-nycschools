//
//  ListOfSchoolsViewModel.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import Foundation

private let schoolsPageSize = 1000
private let refreshTriggerOffset = 5

class ListOfSchoolsViewModel {
    private var schoolCellModels = [SchoolCellViewModel]()
    private var filteredSchoolCellModels: [SchoolCellViewModel]?
    
    private(set) var isLoading = false
    
    var onDataUpdated: ((_ refreshPaths: [IndexPath]) -> Void)?
    var onFailed: ((_ error: Error) -> Void)?
    
    func loadNextSchoolsIfNeeded(forIndexPath indexPath: IndexPath) {
        if indexPath.row == (schoolCellModels.count - 3) {
            loadNextSchools()
        }
    }
    
    func loadSchools() {
        guard isLoading == false else { return }

        DispatchQueue.global().async {
            self.loadNextSchools(count: schoolsPageSize, offset: 0)
        }
    }
    
    func loadNextSchools() {
        guard isLoading == false else { return }
        
        let offset = self.schoolCellModels.count
        DispatchQueue.global().async {
            self.loadNextSchools(count: schoolsPageSize, offset: offset)
        }
    }
    
    func loadNextSchools(count: Int, offset: Int) {
        let api = SchoolsApi(limit: count, offset: offset)
        ApiClient<[SchoolModel]>.execute(api: api) { (objects, error) in
            self.apiLoadCompleted(api, withModels: objects, error: error)
        }
    }
    
    private func apiLoadCompleted(_ api: SchoolsApi, withModels models: [SchoolModel]?, error: Error?) {
        if let error = error {
            loadSchoolsFailed(error)
        } else {
            appendNewModels(models ?? [], withOffset: api.offset ?? 0)
        }
    }
    
    private func loadSchoolsFailed(_ error: Error) {
        DispatchQueue.main.async {
            self.isLoading = false
            self.onFailed?(error)
        }
    }
    
    private func appendNewModels(_ models: [SchoolModel], withOffset offset: Int) {
        let cellModels = models.map { SchoolCellViewModel(with: $0) }
        var indexPaths = [IndexPath]()
        for i in 0..<(models.count) {
            indexPaths.append(IndexPath(row: offset + i, section: 0))
        }
        
        DispatchQueue.main.async {
            let count = self.schoolCellModels.count
            self.schoolCellModels.removeSubrange(offset..<count)
            self.schoolCellModels.append(contentsOf: cellModels)
            self.isLoading = false
            self.onDataUpdated?(indexPaths)
        }
    }
    
    func numberOfRows() -> Int {
        return filteredSchoolCellModels?.count ?? schoolCellModels.count
    }
    
    func cellModel(at indexPath: IndexPath) -> SchoolCellViewModel {
        return (filteredSchoolCellModels ?? schoolCellModels)[indexPath.row]
    }
    
    func filterSchools(forText text: String?) {
        if let text = text?.trimmingCharacters(in: .whitespacesAndNewlines).lowercased(), text.count > 0 {
            let comps = text.split(separator: " ")
            filteredSchoolCellModels = schoolCellModels.filter {
                for c in comps where !$0.school.matches(searchText: String(c)) {
                    return false
                }
                return true
            }
        } else {
            filteredSchoolCellModels = nil
        }
    }
    
    func createSchoolDetailsViewModel(at indexPath: IndexPath) -> SchoolDetailsViewModel {
        return SchoolDetailsViewModel(schoolModel: (filteredSchoolCellModels ?? schoolCellModels)[indexPath.row].school)
    }
}
