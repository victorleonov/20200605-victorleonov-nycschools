//
//  ListOfSchoolsViewController.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import UIKit

private let schoolTableViewCellId = "schoolTableViewCellId"

class ListOfSchoolsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let viewModel = ListOfSchoolsViewModel()
    let searchController = UISearchController(searchResultsController: nil)
    
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshSchools), for: .valueChanged)
        
        bindViewModel()
        configureSearchController()
        
        showActivityIndicator()
        viewModel.loadNextSchools()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let sel = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: sel, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    func bindViewModel() {
        viewModel.onDataUpdated = { [weak self] (refreshIndexPaths) in
            if refreshIndexPaths.count > 0 {
                self?.tableView.reloadData()
                self?.hideActivityIndicator()
            }
        }
        viewModel.onFailed = { [weak self] (error) in
            self?.showAlert(withTitle: "Error", body: "Please try again.")
            self?.hideActivityIndicator()
        }
    }
    
    func showActivityIndicator() {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator() {
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
        
        refreshControl.endRefreshing()
    }
    
    func configureSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Schools"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    @objc func refreshSchools() {
        viewModel.loadSchools()
    }
}

extension ListOfSchoolsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: schoolTableViewCellId, for: indexPath)
        if let cell = cell as? SchoolTableViewCell {
            cell.configure(using: viewModel.cellModel(at: indexPath))
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        viewModel.loadNextSchoolsIfNeeded(forIndexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !viewModel.isLoading, let vc = storyboard?.instantiateViewController(ofType: SchoolDetailsViewController.self) {
            vc.viewModel = viewModel.createSchoolDetailsViewModel(at: indexPath)
            navigationController?.pushViewController(vc, animated: true)
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}

extension ListOfSchoolsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.filterSchools(forText: searchController.searchBar.text)
        tableView.reloadData()
    }
}
