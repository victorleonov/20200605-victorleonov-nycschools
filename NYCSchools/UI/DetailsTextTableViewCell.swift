//
//  DetailsTextTableViewCell.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import UIKit

class DetailsTextTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}
