//
//  SchoolTableViewCell.swift
//  NYCSchools
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var subtitle: UILabel!

    func configure(using model: SchoolCellViewModel) {
        label.text = model.school.name
        subtitle.text = model.school.fullAddress
    }
}
