//
//  ListOfSchoolsViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by Victor Leonov on 6/5/20.
//  Copyright © 2020 Victor Leonov. All rights reserved.
//

import XCTest
@testable import NYCSchools

class ListOfSchoolsViewModelTests: XCTestCase {
    var viewModel: ListOfSchoolsViewModel?
    
    override func setUp() {
        viewModel = ListOfSchoolsViewModel()
    }
    
    override func tearDown() {
        viewModel = nil
    }
    
    func testLoadNextSchools() {
        let expect = expectation(description: "ListOfSchoolsViewModelTests:testLoadNextSchools")
        viewModel?.onDataUpdated = { (_) in
            expect.fulfill()
        }
        viewModel?.loadNextSchools()
        waitForExpectations(timeout: 30.0, handler: nil)
        
        let count = viewModel?.numberOfRows() ?? 0
        XCTAssertTrue(count > 0)
        XCTAssertNotNil(viewModel?.cellModel(at: IndexPath(row: 0, section: 0)))
        XCTAssertNotNil(viewModel?.cellModel(at: IndexPath(row: count - 1, section: 0)))
    }
    
    func testLoadNextSchoolsWithParams() {
        let limit = 5
        var offset = 0
        
        let expect1 = expectation(description: "ListOfSchoolsViewModelTests:testLoadNextSchoolsWithParams1")
        viewModel?.onDataUpdated = { (_) in
            expect1.fulfill()
        }
        viewModel?.loadNextSchools(count: limit, offset: offset)
        waitForExpectations(timeout: 30.0, handler: nil)
        
        let count = viewModel?.numberOfRows() ?? 0
        XCTAssertTrue(count == limit)
        offset += count
        
        let expect2 = expectation(description: "ListOfSchoolsViewModelTests:testLoadNextSchoolsWithParams2")
        viewModel?.onDataUpdated = { (_) in
            expect2.fulfill()
        }
        viewModel?.loadNextSchools(count: limit, offset: offset)
        waitForExpectations(timeout: 30.0, handler: nil)
        
        XCTAssertTrue((viewModel?.numberOfRows() ?? 0) == (offset + limit))
    }
    
    func testFilterSchools() {
        let expect = expectation(description: "ListOfSchoolsViewModelTests:testFilterSchools")
        viewModel?.onDataUpdated = { (_) in
            expect.fulfill()
        }
        viewModel?.loadNextSchools()
        waitForExpectations(timeout: 30.0, handler: nil)
        
        let count = viewModel?.numberOfRows() ?? 0
        viewModel?.filterSchools(forText: "11225")
        let newCount = viewModel?.numberOfRows() ?? 0
        XCTAssertTrue(newCount > 0)
        XCTAssertTrue(newCount != count)
    }
}
